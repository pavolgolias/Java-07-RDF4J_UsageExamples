package sk.pavol.golias.rdf4j.examples;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.http.HTTPRepository;

// First real example - connection to local RDF4J database, putting new tripples and obtaining them back

// http://docs.rdf4j.org/server-workbench-console/
//
// RDF4J SERVER -  is a database management application: it provides HTTP access to RDF4J repositories, exposing them as SPARQL
// endpoints. RDF4J Server is meant to be accessed by other applications. Apart from some functionality to view the server’s log
// messages, it doesn’t provide any user oriented functionality.
//
// RDF4J WORKBENCH -  provides a web interface for querying, updating and exploring the repositories of an RDF4J Server.
//
// The RDF4J CONSOLE is a command-line application for interacting with RDF4J. It can be used to create and use local RDF databases, or it can connect to a running RDF4J Server.
public class Example9 {
    public static void main(String[] args) {
        String rdf4jServer = "http://localhost:8080/rdf4j-server/";
        String repositoryID = "first-example";
        Repository repository = new HTTPRepository(rdf4jServer, repositoryID);
        repository.initialize();

        // create some dummy data
        ValueFactory factory = repository.getValueFactory();
        IRI context1 = factory.createIRI("http://example.org/context1");
        IRI context2 = factory.createIRI("http://example.org/context2");
        IRI creator = factory.createIRI("http://example.org/ontology/creator");
        IRI alice = factory.createIRI("http://example.org/people/alice");
        IRI bob = factory.createIRI("http://example.org/people/bob");
        IRI name = factory.createIRI("http://example.org/ontology/name");
        IRI person = factory.createIRI("http://example.org/ontology/Person");
        Literal bobsName = factory.createLiteral("Bob");
        Literal alicesName = factory.createLiteral("Alice");

        try (RepositoryConnection connection = repository.getConnection()) {
            /*connection.add(alice, RDF.TYPE, person, context1);
            connection.add(alice, name, alicesName, context1);
            connection.add(context1, creator, alicesName);
            connection.add(context2, creator, bobsName);
            connection.add(bob, RDF.TYPE, person, context2);
            connection.add(bob, name, bobsName, context2);*/

            System.out.println("Loaded queries: " + QueryResults.asModel(connection.getStatements(null, null, null)));
        }

    }
}
