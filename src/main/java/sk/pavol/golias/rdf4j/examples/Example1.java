package sk.pavol.golias.rdf4j.examples;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

// tutorial: http://docs.rdf4j.org/getting-started/
public class Example1 {
    public static void main(String[] args) {
        Repository repository = new SailRepository(new MemoryStore());
        repository.initialize();

        String namespace = "http://example.org/";
        ValueFactory valueFactory = repository.getValueFactory();
        IRI john = valueFactory.createIRI(namespace, "john");

        try (RepositoryConnection connection = repository.getConnection()) {
            connection.add(john, RDF.TYPE, FOAF.PERSON);
            connection.add(john, RDFS.LABEL, valueFactory.createLiteral("John"));

            RepositoryResult<Statement> statements = connection.getStatements(null, null, null);
            // The object returned by getStatements() is a RepositoryResult, which is a type of Iteration: it allows you
            // to process the result in a streaming fashion, using its hasNext() and next() methods. Almost all the
            // methods that retrieve data from a Repository return a type of Iteration: they are very useful for
            // processsing very large result sets, because they do not require that the entire result is kept in memory
            // all at the same time.

            // ... but we use RDF4J Java Collection interface called MODEL for now ...
            Model model = QueryResults.asModel(statements);
            // This retrieves all the statements from the supplied Iteration, and puts them in a Model. It also closes
            // the Iteration for you.

            model.setNamespace(RDF.NS);
            model.setNamespace(RDFS.NS);
            model.setNamespace(FOAF.NS);
            model.setNamespace("ex", namespace);

            System.out.println("\n\nN-TRIPLES:");
            Rio.write(model, System.out, RDFFormat.NTRIPLES);
            System.out.println("\n\nTURTLE:");
            Rio.write(model, System.out, RDFFormat.TURTLE);
            System.out.println("\n\nRDF/XML:");
            Rio.write(model, System.out, RDFFormat.RDFXML);
            System.out.println("\n\nRDF/JSON:");
            Rio.write(model, System.out, RDFFormat.RDFJSON);

            // Rio (which stands for “RDF I/O”) is the RDF4J parser/writer toolkit. We can just pass it our model,
            // specify the outputstream, and the format in which we’d like it sent, and Rio does the rest.
        }
    }
}
