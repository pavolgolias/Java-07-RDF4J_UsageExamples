package sk.pavol.golias.rdf4j.examples;

import org.eclipse.rdf4j.model.*;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.*;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandler;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.function.Function;

// SOURCE: http://docs.rdf4j.org/programming/#_using_a_repository_repositoryconnections

// There are 3 types of queries:
//  - TUPLE QUERY: is a set of tuples (or variable bindings), where each tuple represents a solution of a query. This
//      type of query is commonly used to get specific values (URIs, blank nodes, literals) from the stored RDF data.
//      SPARQL SELECT queries are tuple queries.
//  - GRAPH QUERY: The result of graph queries is an RDF graph (or set of statements). This type of query is very useful
//      for extracting sub-graphs from the stored RDF data, which can then be queried further, serialized to an RDF
//      document, etc. SPARQL CONSTRUCT and DESCRIBE queries are graph queries.
//  - BOOLEAN QUERY: The result of boolean queries is a simple boolean value, i.e. true or false. This type of query can
//      be used to check if a repository contains specific information. SPARQL ASK queries are boolean queries.

@SuppressWarnings("unused")
public class Example5 {
    private static Repository repository;
    private static String namespace = "http://example.org/";

    public static void main(String[] args) {
        // Common in memory repository for all examples
        repository = new SailRepository(new MemoryStore());
        repository.initialize();

        try (RepositoryConnection connection = repository.getConnection()) {
            ValueFactory valueFactory = repository.getValueFactory();

            IRI bob = valueFactory.createIRI(namespace, "bob");
            IRI name = valueFactory.createIRI(namespace, "name");
            IRI email = valueFactory.createIRI(namespace, "mail");
            Literal bobsName = valueFactory.createLiteral("Bob");
            Literal bobsEmail = valueFactory.createLiteral("bob@email.com");
            Statement nameStatement = valueFactory.createStatement(bob, name, bobsName);
            Statement emailStatement = valueFactory.createStatement(bob, email, bobsEmail);
            Statement typeStatement = valueFactory.createStatement(bob, RDF.TYPE, FOAF.PERSON);

            connection.add(nameStatement);
            connection.add(emailStatement);
            connection.add(typeStatement);

            connection.setNamespace("ex", namespace);
        }

        // Executing examples:

        //addRdfToRepository();
        //evaluatingTupleQuery();
        //evaluatingGraphQuery();
        //preparingAndReusingQueries();
        //creatingRetrievingAndRemovingStatements();
        //usingNamesGraphsOrContext();
        workingWithModelsAndCollections();
    }

    private static void addRdfToRepository() {
        File file = new File("/path/to/example.rdf");
        String baseURI = "http://example.org/example/local";

        try (RepositoryConnection con = repository.getConnection()) {
            con.add(file, baseURI, RDFFormat.RDFXML);
            URL url = new URL("http://example.org/example/remote.rdf");
            con.add(url, url.toString(), RDFFormat.RDFXML);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void evaluatingTupleQuery() {
        try (RepositoryConnection connection = repository.getConnection()) {
            String queryString = "SELECT ?x ?y WHERE { ?x ?p ?y } ";
            TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL, queryString);

            try (TupleQueryResult result = tupleQuery.evaluate()) {
                System.out.println("Binding names: " + result.getBindingNames());

                while (result.hasNext()) {  // iterate over the result
                    BindingSet bindingSet = result.next();
                    Value valueOfX = bindingSet.getValue("x");
                    Value valueOfY = bindingSet.getValue("y");

                    System.out.println("valueOfX:" + valueOfX + " | valueOfY:" + valueOfY);
                }
            }

            //Another approach - using binding names
            try (TupleQueryResult result = tupleQuery.evaluate()) {
                System.out.println("Binding names: " + result.getBindingNames());

                List<String> bindingNames = result.getBindingNames();
                while (result.hasNext()) {
                    BindingSet bindingSet = result.next();
                    Value firstValue = bindingSet.getValue(bindingNames.get(0));
                    Value secondValue = bindingSet.getValue(bindingNames.get(1));

                    System.out.println("firstValue:" + firstValue + " | secondValue:" + secondValue);
                }
            }

            //Another approach - converting to List
            List<BindingSet> resultList;
            try (TupleQueryResult result = tupleQuery.evaluate()) {
                resultList = QueryResults.asList(result);
            }
            System.out.println("Result list: " + resultList);
        }

        // to open a connection, create and evaluate a SPARQL SELECT query, and then put that query’s result in a list,
        // we can do the following:
        try {
            Function<TupleQueryResult, List<BindingSet>> function = QueryResults::asList;    // this have to be explicitly created or compilation error occur
            List<BindingSet> results = Repositories.tupleQuery(repository, "SELECT * WHERE {?s ?p ?o }", function);

            System.out.println("One line query with connection opening: " + results);
        } catch (Exception e) {
            System.err.println("Error occurred while executing query.");
        }

        // Another example - SPARQLResultsCSVWriter is a TupleQueryResultHandler implementation that writes SPARQL Results as comma-separated values
        try (RepositoryConnection connection = repository.getConnection()) {
            String queryString = "SELECT * WHERE {?x ?p ?y }";
            // http://docs.rdf4j.org/javadoc/latest/?org/eclipse/rdf4j/query/TupleQueryresultHandler.html
            // connection.prepareTupleQuery(queryString).evaluate(new SPARQLResultsJSONWriter(System.out));   // TODO find dependency
        }

    }

    private static void evaluatingGraphQuery() {
        try (RepositoryConnection connection = repository.getConnection()) {
            GraphQueryResult graphResult = connection
                    .prepareGraphQuery("CONSTRUCT { ?s ?p ?o } WHERE {?s ?p ?o }")
                    .evaluate();

            // for graph queries the query solutions are RDF statements, so a GraphQueryResult iterates over Statement objects:
            while (graphResult.hasNext()) {
                Statement statement = graphResult.next();
                System.out.println("Graph query result: " + statement);
            }

            // Another approach - converting to MODEL (= Java Collection of statements)
            graphResult = connection
                    .prepareGraphQuery("CONSTRUCT { ?s ?p ?o } WHERE {?s ?p ?o }")
                    .evaluate();

            Model resultModel = QueryResults.asModel(graphResult);
            System.out.println("Graph query result as model: " + resultModel);

            RDFWriter writer = Rio.createWriter(RDFFormat.TURTLE, System.out);
            connection.prepareGraphQuery(QueryLanguage.SPARQL, "CONSTRUCT {?s ?p ?o } WHERE {?s ?p ?o } ")
                    .evaluate(writer);
        }

        // Another approach - pne row
        Model model = Repositories.graphQuery(repository, "CONSTRUCT WHERE {?s ?p ?o}", QueryResults::asModel);
        System.out.println("One row graph query: " + model);
    }

    private static void preparingAndReusingQueries() {
        // A Query object, once created, can be (re)used. For example, we can evaluate a Query object, then add some
        // data to our repository, and evaluate the same query again.

        try (RepositoryConnection con = repository.getConnection()) {
            String pre = "PREFIX ex: <" + namespace + ">\n";
            // First, prepare a query that retrieves all names of persons
            TupleQuery nameQuery = con.prepareTupleQuery(pre + "SELECT ?name WHERE { ?person ex:name ?name . }");

            // Then, prepare another query that retrieves all e-mail addresses of persons:
            TupleQuery mailQuery = con
                    .prepareTupleQuery(pre + "SELECT ?mail WHERE { ?person ex:mail ?mail ; ex:name ?name . }");

            // Evaluate the first query to get all names
            try (TupleQueryResult nameResult = nameQuery.evaluate()) {
                // Loop over all names, and retrieve the corresponding e-mail address.
                while (nameResult.hasNext()) {
                    BindingSet bindingSet = nameResult.next();
                    Value name = bindingSet.getValue("name");
                    System.out.println("Found name: " + name);

                    // Retrieve the matching mailbox, by setting the binding for
                    // the variable 'name' to the retrieved value. Note that we
                    // can set the same binding name again for each iteration, it will
                    // overwrite the previous setting.
                    //mailQuery.setBinding("name", name);
                    try (TupleQueryResult mailResult = mailQuery.evaluate()) {
                        while (mailResult.hasNext()) {
                            BindingSet bindingSet1 = mailResult.next();
                            Value mail = bindingSet1.getValue("mail");
                            System.out.println("Found mail: " + mail);
                        }
                    }
                }
            }

            //Another example
            ValueFactory factory = repository.getValueFactory();
            String keyword = "foobar";
            TupleQuery keywordQuery = con
                    .prepareTupleQuery("SELECT ?document WHERE { ?document ex:keyword ?keyword . }");
            keywordQuery.setBinding("keyword", factory.createLiteral(keyword));
            TupleQueryResult keywordQueryResult = keywordQuery.evaluate();
        }
    }

    private static void creatingRetrievingAndRemovingStatements() {
        // we can simply reuse the predefined constants found in the org.eclipse.rdf4j.model.vocabulary package,
        // and using the ModelBuilder utility you can very quickly create collections of statements without ever
        // touching a ValueFactory.

        ValueFactory f = repository.getValueFactory();
        IRI alice = f.createIRI("http://example.org/people/alice");
        IRI bob = f.createIRI("http://example.org/people/bob");
        IRI name = f.createIRI("http://example.org/ontology/name");
        IRI person = f.createIRI("http://example.org/ontology/Person");
        Literal bobsName = f.createLiteral("Bob");
        Literal alicesName = f.createLiteral("Alice");

        try (RepositoryConnection connection = repository.getConnection()) {
            connection.add(alice, RDF.TYPE, person);
            connection.add(alice, name, alicesName);
            connection.add(bob, RDF.TYPE, person);
            connection.add(bob, name, bobsName);

            try (RepositoryResult<Statement> statements = connection.getStatements(alice, null, null)) {
                while (statements.hasNext()) {
                    System.out.println("All statements about Alice: " + statements.next());
                }
            }

            // remove statements
            connection.remove(alice, name, alicesName);
            connection.remove(alice, null, null);
        }
    }

    private static void usingNamesGraphsOrContext() {
        // RDF4J supports the notion of context, which you can think of as a way to group sets of statements together
        // through a single group identifier (this identifier can be a blank node or a URI).

        String location = "http://example.org/example/";
        File file = new File(
                "D:\\School_Ing\\Diplomovka\\Projects\\Java-07-RDF4J_UsageExamples\\src\\main\\resources\\xml\\rdf-file-example-1.xml");
        IRI context = repository.getValueFactory().createIRI(location);

        try (RepositoryConnection connection = repository.getConnection()) {
            connection.add(file, location, RDFFormat.RDFXML, context);

            try (RepositoryResult<Statement> result = connection.getStatements(null, null, null, context)) {
                while (result.hasNext()) {
                    Statement st = result.next();
                    System.out.println("Loaded statement: " + st.toString());
                }
            }

            // Export all statements in the context to System.out, in RDF/XML format
            RDFHandler writer = Rio.createWriter(RDFFormat.RDFXML, System.out);
            connection.export(writer, context);
            // Remove all statements in the context from the repository
            connection.clear(context);
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // ANOTHER CONTEXT EXAMPLE
        ValueFactory factory = repository.getValueFactory();
        IRI context1 = factory.createIRI("http://example.org/context1");
        IRI context2 = factory.createIRI("http://example.org/context2");
        IRI creator = factory.createIRI("http://example.org/ontology/creator");
        IRI alice = factory.createIRI("http://example.org/people/alice");
        IRI bob = factory.createIRI("http://example.org/people/bob");
        IRI name = factory.createIRI("http://example.org/ontology/name");
        IRI person = factory.createIRI("http://example.org/ontology/Person");
        Literal bobsName = factory.createLiteral("Bob");
        Literal alicesName = factory.createLiteral("Alice");

        Repository repository = new SailRepository(new MemoryStore());
        repository.initialize();

        try (RepositoryConnection connection = repository.getConnection()) {
            connection.add(alice, RDF.TYPE, person, context1);
            connection.add(alice, name, alicesName, context1);

            connection.add(context1, creator, alicesName);
            connection.add(context2, creator, bobsName);

            connection.add(bob, RDF.TYPE, person, context2);
            connection.add(bob, name, bobsName, context2);

            // Get all statements in either context1 or context2
            RepositoryResult<Statement> result = connection.getStatements(null, null, null, context1, context2);
            while (result.hasNext()) {
                System.out.println("Context 1 && 2: " + result.next());
            }

            // Get all statements that do not have an associated context
            result = connection.getStatements(null, null, null, (Resource) null);
            while (result.hasNext()) {
                System.out.println("No context: " + result.next());
            }

            // Get all statements that do not have an associated context, or that are in context1
            result = connection.getStatements(null, null, null, null, context1);
            while (result.hasNext()) {
                System.out.println("No context && 1: " + result.next());
            }

            // getStatements(null, null, null); is not the same as getStatements(null, null, null, (Resource)null);
            // The former (without any context id parameter) retrieves all statements in the repository, ignoring any
            // context information. The latter, however, only retrieves statements that explicitly do not have any
            // associated context.
        }

    }

    private static void workingWithModelsAndCollections() {
        try (RepositoryConnection connection = repository.getConnection()) {
            IRI bob = repository.getValueFactory().createIRI(namespace, "bob");

            RepositoryResult<Statement> statements = connection.getStatements(bob, null, null);
            Model aboutBob = QueryResults.asModel(statements);
            System.out.println("deleting aboutBob " + aboutBob);
            connection.remove(aboutBob);
            // connection.remove(connection.getStatements(bob, null, null));    // shorter

            statements = connection.getStatements(bob, null, null);
            Model aboutBob2 = QueryResults.asModel(statements);
            System.out.println("retrieved aboutBob " + aboutBob2);

            connection.add(aboutBob);
            // we can use this to add statements + model has its ModelBuilder to build statements
            System.out
                    .println("current statements: " + QueryResults.asModel(connection.getStatements(bob, null, null)));

            //Collections
            // Model rdfList = Connections.getRDFCollection(connection, v, new LinkedHashModel());
            // Connections.consumeRDFCollection(connection, node, st -> { /* ... do something with the triples forming the collection */});

        }
    }
}
