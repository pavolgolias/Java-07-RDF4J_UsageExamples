package sk.pavol.golias.rdf4j.examples;

public class Example7 {
    // JUST NOTES

    // MULTITHREADED-REPOSITORY-ACCESS - http://docs.rdf4j.org/programming/#_multithreaded_repository_access
    // The Repository API supports multithreaded access to a store: multiple concurrent threads can obtain connections to a Repository
    // and query and performs operations on it simultaneously (though, depending on the transaction isolation level, access may occassionally
    // block as a thread needs exclusive access).
    //
    // The Repository object is thread-safe, and can be safely shared and reused across multiple threads (a good way to do this is via a RepositoryProvider).
    //
    // However, it is very important to note that RepositoryConnection is not guaranteedto be thread-safe. This means that you should
    // not try to share a single RepositoryConnection over multiple threads. Instead, ensure that each thread creates and uses its own
    // `RepositoryConnection`s, and use transaction isolation levels to control visibility of concurrent updates between threads.



    // PARSING-AND-WRITING-RDF-WITH-RIO - http://docs.rdf4j.org/programming/#_parsing_and_writing_rdf_with_rio
    // The RDF4J framework includes a set of parsers and writers called Rio. Rio (“RDF I/O”) is a toolkit that can be
    // used independently from the rest of RDF4J. In this chapter, we will take a look at various ways to use Rio to
    // parse from or write to an RDF document. We will show how to do a simple parse and collect the results, how to
    // count the number of triples in a file, how to convert a file from one syntax format to another, and how to
    // dynamically create a parser for the correct syntax format.
    //
    // If you use RDF4J via the Repository API, then
    // typically you will not need to use the parsers directly: you simply supply the document (either via a URL, or as
    // a File, InputStream or Reader object) to the RepositoryConnection and the parsing is all handled internally.
    // However, sometimes you may want to parse an RDF document without immediately storing it in a triplestore. For
    // those cases, you can use Rio directly.
    //
    // Usages: 1.parsing RDF file and collecting all statements
    //  2. parsings RDF file and getting the number of statements (or creating any other functionality by creating own custom RDFHandler)
    //  3. detection of file format
    //  4. read and write RDF in different syntax
    //  5. convert RDF to different syntax without need to load all statements for RDF - just read and write them directly



    // Customization of SAILs - http://docs.rdf4j.org/programming/#_customization_sails
    // Usages:
    //  1. Full text indexing and search with the Lucene SAIL
    //  2. SPARQL SPIN support

}
