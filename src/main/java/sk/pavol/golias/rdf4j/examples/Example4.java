package sk.pavol.golias.rdf4j.examples;

import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.config.RepositoryConfig;
import org.eclipse.rdf4j.repository.manager.LocalRepositoryManager;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryProvider;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.sail.config.SailImplConfig;
import org.eclipse.rdf4j.sail.inferencer.fc.CustomGraphQueryInferencer;
import org.eclipse.rdf4j.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.eclipse.rdf4j.sail.inferencer.fc.config.ForwardChainingRDFSInferencerConfig;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;

import java.io.File;

// SOURCE: http://docs.rdf4j.org/programming/#_the_repository_api
// REPOSITORY API - The Repository API is the central access point for RDF4J-compatible RDF databases (a.k.a. triplestores), as well as for SPARQL endpoints.
// Three main types of repository:
//  - SailRepository - works with database
//  - HTTPRepository - acts as a proxy to a repository available on a remote RDF4J Server, accessible through HTTP
//  - SPARQLRepository - acts as a proxy to any remote SPARQL endpoint (whether that endpoint is implemented using RDF4J or not).
public class Example4 {
    public static void main(String[] args) {
        //mainMemoryRepositoryExamples();
        //nativeRepositoryExamples();
        //rdfSchemaInferencerRepository();
        //customInferencingRuleRepository();

        // it is also possibl to access server-side repositories:
        //  - http://docs.rdf4j.org/programming/#_accessing_a_server_side_repository
        //  - http://docs.rdf4j.org/programming/#_accessing_a_server_side_repository

        //localRepositoryManagerExample();
        remoteRepositoryManagerExample();

    }

    private static void remoteRepositoryManagerExample() {
        //  it is also possible to use a RemoteRepositoryManager, using it to create and manage repositories residing on
        // a remotely running RDF4J Server.
        String serverUrl = "http://localhost:8080/rdf4j-server";
        RemoteRepositoryManager remoteRepositoryManager = new RemoteRepositoryManager(serverUrl);
        remoteRepositoryManager.initialize();
        System.out.println(remoteRepositoryManager);

        // once initialized, the RemoteRepositoryManager can be used in the same fashion as the LocalRepositoryManager:
        // creating new repositories, requesting references to existing repositories, and so on.

        String url = "http://localhost:8080/rdf4j-server";
        // Creates a RepositoryManager, if not already created, that will be shutdown when the JVM exits cleanly.
        // RepositoryProvider creates and keeps a singleton instance of RepositoryManager
        RepositoryManager manager = RepositoryProvider.getRepositoryManager(url);
        System.out.println(manager);

        //RepositoryManagerFederator // Utility class for handling the details of federating "user repositories" managed by a RepositoryManager.
    }

    private static void localRepositoryManagerExample() {
        // RepositoryManager - A manager for Repositorys. Every RepositoryManager has one SYSTEM repository and zero or
        // more "user repositories". The SYSTEM repository contains data that describes the configuration of the other
        // repositories (their IDs, which implementations of the Repository API to use, access rights, etc.). The other
        // repositories are instantiated based on this configuration data.
        // The RepositoryManager comes in two flavours: the LocalRepositoryManager and the RemoteRepositoryManager.

        // create a configuration for the SAIL stack
        SailImplConfig backendConfig = new MemoryStoreConfig(true);

        // stack an inferencer config on top of our backend-config
        backendConfig = new ForwardChainingRDFSInferencerConfig(backendConfig);

        // create a configuration for the repository implementation
        SailRepositoryConfig repositoryTypeSpec = new SailRepositoryConfig(backendConfig);
        final String repositoryId = "test-db";
        RepositoryConfig repConfig = new RepositoryConfig(repositoryId, repositoryTypeSpec);

        File baseDir = new File("D:\\School_Ing\\Diplomovka\\Java-07-RDF4J_UsageExamples_Storage\\");
        LocalRepositoryManager manager = new LocalRepositoryManager(baseDir);
        manager.initialize();

        manager.addRepositoryConfig(repConfig);

        Repository repository = manager.getRepository("no-repository-rest");
        System.out.println(repository);
        repository = manager.getRepository(repositoryId);
        System.out.println(repository);
    }

    private static void customInferencingRuleRepository() {
        String pre = "PREFIX : <http://foo.org/bar#>\n";
        String rule = pre + "CONSTRUCT { ?p :relatesTo :Cryptography } WHERE { { :Bob ?p :Alice } "
                + "UNION { :Alice ?p :Bob } }";
        String match = pre + "CONSTRUCT { ?p :relatesTo :Cryptography } WHERE { ?p :relatesTo :Cryptography }";
        Repository repo = new SailRepository(
                new CustomGraphQueryInferencer(new MemoryStore(), QueryLanguage.SPARQL, rule, match));
        repo.initialize();

        // see example at http://docs.rdf4j.org/programming/#_creating_a_repository_with_a_custom_inferencing_rule
        // for what it can be used
    }

    private static void rdfSchemaInferencerRepository() {
        //  ForwardChainingRDFSInferencer is a generic RDF Schema inferencer
        Repository repo = new SailRepository(new ForwardChainingRDFSInferencer(new MemoryStore()));
        repo.initialize();
    }

    private static void nativeRepositoryExamples() {
        // A Native RDF Repository does not keep its data in main memory, but instead stores it directly to disk (in a
        // binary format optimized for compact storage and fast retrieval). It is an efficient, scalable and fast
        // solution for RDF storage of datasets that are too large to keep entirely in memory.

        File dataDirectory = new File("D:\\School_Ing\\Diplomovka\\Java-07-RDF4J_UsageExamples_Storage\\");
        String indexes = "spoc,posc,cosp";
        Repository repo = new SailRepository(new NativeStore(dataDirectory, indexes));
        repo.initialize();
    }

    private static void mainMemoryRepositoryExamples() {
        // The following code creates and initializes a non-inferencing main-memory repository:
        Repository repository = new SailRepository(new MemoryStore());
        repository.initialize();
        repository.shutDown();

        // Creating a main memory RDF Repository
        File dataDirectory = new File("D:\\School_Ing\\Diplomovka\\Java-07-RDF4J_UsageExamples_Storage\\");
        MemoryStore memStore = new MemoryStore(dataDirectory);
        memStore.setSyncDelay(1000L);// by usage of this, bursts of transaction events can be combined in one file sync.
        repository = new SailRepository(memStore);
        repository.initialize();
        repository.shutDown();
    }
}
