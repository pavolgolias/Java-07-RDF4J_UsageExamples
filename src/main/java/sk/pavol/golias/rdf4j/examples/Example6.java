package sk.pavol.golias.rdf4j.examples;

import org.eclipse.rdf4j.IsolationLevels;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import java.io.File;
import java.io.IOException;

// http://docs.rdf4j.org/programming/#_transactions

// The RepositoryConnection interface supports a full transactional mechanism that allows one to group modification
// operations together and treat them as a single update: before the transaction is committed, none of the operations
// in the transaction has taken effect, and after, they all take effect. If something goes wrong at any point during a
// transaction, it can be rolled back so that the state of the repository is the same as before the transaction started.
// Bundling update operations in a single transaction often also improves update performance compared to multiple
// smaller transactions.

public class Example6 {
    public static void main(String[] args) {
        Repository repository = new SailRepository(new MemoryStore());
        repository.initialize();

        RepositoryConnection connection = repository.getConnection();
        IRI context = repository.getValueFactory().createIRI("http://example.org/example/");
        try {
            if (connection.isActive()) {
                System.out.println("CONNECTION IS ACTIVE");
            }

            String baseURI = "http://example.org/example1/";
            File file = new File("D:\\School_Ing\\Diplomovka\\Projects\\Java-07-RDF4J_UsageExamples"
                    + "\\src\\main\\resources\\xml\\rdf-file-example-1.xml");

            // http://docs.rdf4j.org/programming/#_transaction_isolation_levels
            // A transaction isolation level dictates who can ‘see’ the updates that are perfomed as part of the
            // transaction while that transaction is active, as well as how concurrent transactions interact with each other.
            // A transaction isolation level is a sort of contract, that is, a set of guarantees of what will minimally happen while the transaction is active.
            connection.begin(IsolationLevels.SNAPSHOT_READ);
            connection.add(file, baseURI, RDFFormat.RDFXML, context);
            connection.commit();

            System.out.println("Added statements: "
                    + QueryResults.asModel(connection.getStatements(null, null, null, context)));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (RepositoryException e) {
            System.err.println("Something went wrong during operation");
            connection.rollback();
        } finally {
            connection.close();
        }

        // using Repositories.consume(), we do not explicitly begin or commit a transaction. We don’t even open and close
        // a connection explicitly – this is all handled internally. The method also ensures that the transaction is rolled back if an exception occurs.
        // This pattern is useful for simple transactions, however as we’ve seen above, we sometimes do need to explicitly
        // call begin(), especially if we want to modify the transaction isolation level.
        ValueFactory f = repository.getValueFactory();
        IRI bob = f.createIRI("urn:bob");
        Repositories.consume(repository, conn -> {
            conn.add(bob, RDF.TYPE, FOAF.PERSON);
            conn.add(bob, RDFS.LABEL, f.createLiteral("Bob"));
        });
    }

    // In the above example, we use a transaction to add two files to the repository (in this example only one file). Only if both files can be
    // successfully added will the repository change. If one of the files can not be added (for example because it can
    // not be read), then the entire transaction is cancelled and none of the files is added to the repository.

    // A RepositoryConnection only supports one active transaction at a time. You can check at any time whether a
    // transaction is active on your connection by using the isActive() method. If you need concurrent transactions,
    // you will need to use several separate RepositoryConnections.

}
