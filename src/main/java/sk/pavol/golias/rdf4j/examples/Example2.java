package sk.pavol.golias.rdf4j.examples;

import org.eclipse.rdf4j.model.*;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import java.util.Optional;

// SOURCE: http://docs.rdf4j.org/programming/#_the_rdf_model_api
public class Example2 {
    public static void main(String[] args) {
        ValueFactory valueFactory = SimpleValueFactory.getInstance();   // better is to obtain this from Repozitory

        IRI bob = valueFactory.createIRI("http://example.org/bob");
        IRI name = valueFactory.createIRI("http://example.org/name");
        Literal bobsName = valueFactory.createLiteral("Bob");
        Statement nameStatement = valueFactory.createStatement(bob, name, bobsName);
        Statement typeStatement = valueFactory.createStatement(bob, RDF.TYPE, FOAF.PERSON);
        // VOCABULARY http://docs.rdf4j.org/javadoc/latest/?org/eclipse/rdf4j/model/vocabulary/package-summary.html

        System.out.println("Statements examples: ");
        System.out.println(nameStatement.toString());
        System.out.println(typeStatement.toString());
        System.out.println("------------------------\n");
        // ---------------------------------------------------

        Model model = new LinkedHashModel();  // Model is an extension of the default Java Collection class java.util.Set<Statement>
        //        model.setNamespace(RDF.NS);
        //        model.setNamespace(FOAF.NS);
        //        model.setNamespace("ex", "http://example.org/");
        model.add(typeStatement);
        //model.add(bob, name, bobsName);
        model.add(bob, FOAF.NAME, bobsName);

        for (Statement statement : model) {
            System.out.println("Statement from model: " + statement);
        }

        // it is possible to get subset of statements and search/filter collection of statements
        for (Statement statement : model.filter(null, RDF.TYPE, FOAF.PERSON)) {
            System.out.println("(1) Filtered from model: " + statement);
        }

        for (Resource person : model.filter(null, RDF.TYPE, FOAF.PERSON).subjects()) {
            // get the name of the person (if it exists)
            Optional<Literal> personsName = Models.objectLiteral(model.filter(person, FOAF.NAME, null));
            if (personsName.isPresent()) {
                System.out.println("(2) Filtered from model (" + person + "): " + personsName.get().getLabel());
            } else {
                System.out.println("(2) Filtered from model (" + person + "): NONE");
            }
        }

        // there are 2 types of model - LinkedHashModel and TreeModel - differences are noticeable when dealing with
        // large collections of statements or huge searches

        // MODEL_BUILDER
        ModelBuilder builder = new ModelBuilder();
        builder.setNamespace("ex", "http://example.org/").setNamespace(FOAF.NS);
        builder.namedGraph("ex:graph1")    // add a new named graph to the model
                .subject("ex:john")     // add  several statements about resource ex:john
                .add(FOAF.NAME, "John") // add the triple (ex:john, foaf:name "John") to the named graph
                .add(FOAF.AGE, 42)
                .add(FOAF.MBOX, "john@example.org");

        // add a tripple to the default graph
        builder.defaultGraph().add("ex:graph1", RDF.TYPE, "ex:Graph");
        Model m = builder.build();
        System.out.println(m);
    }
}
