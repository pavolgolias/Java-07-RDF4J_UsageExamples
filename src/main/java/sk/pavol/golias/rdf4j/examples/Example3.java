package sk.pavol.golias.rdf4j.examples;

import org.eclipse.rdf4j.model.*;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.RDFCollections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// SOURCE: http://docs.rdf4j.org/programming/#_rdf_collections
public class Example3 {
    public static void main(String[] args) {
        // Example how to create RDF Collection
        String ns = "http://example.org/";
        ValueFactory factory = SimpleValueFactory.getInstance();

        IRI favouriteLetters = factory.createIRI(ns, "favouriteLetters");   // IRI for ex:favoriteLetters
        IRI john = factory.createIRI(ns, "John");   // IRI for ex:John
        List<Literal> letters = Arrays.asList(factory.createLiteral("A"), factory.createLiteral("B"),
                factory.createLiteral("C"));    // create a list of letters
        Resource head = factory.createBNode();   // create a head resource for our list

        Model aboutJohn = RDFCollections
                .asRDF(letters, head, new LinkedHashModel());   // convert our list and add it to a newly-created Model
        aboutJohn.add(john, favouriteLetters, head);

        System.out.println("Model created:");
        System.out.println(aboutJohn);
        System.out.println();

        // Example how to read RDF Collection
        Resource node = Models.objectResource(aboutJohn.filter(john, favouriteLetters, null)).orElse(null);
        if (node != null) {
            List<Value> values = RDFCollections.asValues(aboutJohn, node, new ArrayList<>());
            values.forEach(s -> System.out.print(s.stringValue() + "|"));
        } else {
            System.out.println("No RDF collection found!");
        }

        // Example how to retrieve the whole RDF Collection
        if (node != null) {
            Model rdfList = RDFCollections.getCollection(aboutJohn, node, new LinkedHashModel());
            System.out.println("\nRetrieved collection:\n" + rdfList);

            // aboutJohn.removeAll(rdfList);                   // remove collection from our model
            RDFCollections.extract(aboutJohn, node, aboutJohn::remove);     // more efficient approach to delete collection
            System.out.println("Updated model: " + aboutJohn);

            aboutJohn.remove(john, favouriteLetters, node); // remove the triple that linked John to the collection
            System.out.println("Updated model: " + aboutJohn);
        }

    }
}
