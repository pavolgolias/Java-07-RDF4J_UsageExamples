package sk.pavol.golias.rdf4j.examples;

public class Example8 {
    // JUST NOTES

    // THE SAIL API - http://docs.rdf4j.org/sail/
    // SAIL API enables to create own implementations of SAIL to provide custom functionality. Of course, there is already created implementations in RDF4J.
    //
    //  RDF4J’s NativeStore and MemoryStore are implementations of such a persistence SAIL, while the ForwardChainingRDFSInferencer
    // (responsible for RDFS inferencing) and LuceneSail (responsible for full-text indexing) are examples of StackableSail implementations.
    //
    // The SAIL API has no knowledge of SPARQL queries. Instead, it operates on a query algebra, that is, an object representation of a (SPARQL) query as provided by the SPARQL query parser.
    // SailConnection has a single evaluate() method, which accepts a org.eclipse.rdf4j.queryalgebra.model.TupleExpr object. This is the object representation of the query as produced by the query parser.

    // RDF4J Binary RDF Format - http://docs.rdf4j.org/rdf4j-binary/
    // RDF4J supports reading and writing a custom binary RDF serialization format. Its main features are reduced
    // parsing overhead and minimal memory requirements (for handling really long literals, amongst other things).

    // The RDF4J Server REST API - http://docs.rdf4j.org/rest-api/#_content_types
    // The RDF4J Server REST API is a HTTP Protocol that covers a fully compliant implementation of the SPARQL 1.1
    // Protocol W3C Recommendation. This ensures that RDF4J Server functions as a fully standards-compliant SPARQL endpoint.
}
